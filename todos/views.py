from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem
# Create your views here.


def show_todo_lists(request):
    todolists = TodoList.objects.all()
    context = {
        "todolist_list": todolists
        }
    return render(
        request, "todo_lists/list.html", context
        )


def show_todo_list(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)

    context = {
        "todolist": todolist
    }
    return render(
        request, "todo_lists/detail.html", context
    )
